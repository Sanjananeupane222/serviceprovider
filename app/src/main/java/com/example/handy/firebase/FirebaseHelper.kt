package com.example.handy.firebase

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import com.example.handy.ui.dashboard.ui.home.model.ServiceModel
import com.example.handy.ui.register.model.UserModel
import com.example.handy.util.networkUtils.DataResource
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit


@AndroidEntryPoint
class FirebaseHelper(private val mAuth: FirebaseAuth, private val database: FirebaseDatabase) :
    AppCompatActivity() {

    companion object {
        const val REFERENCE_USER = "USER"
        const val ROLE = "role"
        const val SERVICES = "Services"
    }

    fun verifyPhoneNumber(
        number: String,
        activity: Activity,
        callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    ) {
        val options = PhoneAuthOptions.newBuilder(mAuth)
            .setPhoneNumber(number)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(activity)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    fun signInWithPhoneAuthCredential(
        credential: PhoneAuthCredential,
        context: Activity,
        firebaseCallback: FirebaseCustomCallbacks
    ) {
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(context) { task ->
                if (task.isSuccessful) {
                    firebaseCallback.genericCallback(DataResource.Success(data = "Successfully Logged In"))
                    val user = task.result?.user
                } else {
                    firebaseCallback.genericCallback(DataResource.Success(data = "Failed to  Log In"))
                }
            }
    }

    fun signUpWithEmail(userModel: UserModel, callbacks: FirebaseCustomCallbacks) {
        val emailAddress = userModel.emailAddress
        val password = userModel.password
        emailAddress?.let { email ->
            password?.let { password ->
                mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            callbacks.genericCallback(
                                DataResource.Success(
                                    data = mAuth.currentUser?.email ?: ""
                                )
                            )
                        } else {
                            callbacks.genericCallback(DataResource.Error(message = task.exception?.message.toString()))
                        }
                    }
            }
        }

    }

    fun signInWithEmailAndPassword(
        emailAddress: String,
        password: String,
        callbacks: FirebaseCustomCallbacks
    ) {
        emailAddress.let { email ->
            password.let { password ->
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        callbacks.genericCallback(
                            DataResource.Success(
                                data = mAuth.currentUser?.email ?: ""
                            )
                        )
                    } else {
                        callbacks.genericCallback(DataResource.Error(message = task.exception?.message.toString()))
                    }
                }
            }
        }
    }

    fun saveUser(userModel: UserModel) {
        val key = userModel.emailAddress?.split("@",".","#","$","[","]")?.get(0) ?: ""
        val userDatabaseReference = database.getReference(REFERENCE_USER).child(key)
        userDatabaseReference.setValue(userModel)
    }


    fun readUser(email: String, callbacks: FirebaseCustomCallbacks) {
        val userDatabaseReference = database.getReference(REFERENCE_USER).child(
            email.split("@",".","#","$","[","]")[0] ?: ""
        )
        val userListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val userDetails = dataSnapshot.getValue(UserModel::class.java)
                userDetails?.let {
                    callbacks.getUserDetails(it)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        userDatabaseReference.addValueEventListener(userListener)
    }

    fun getServices(callback: FirebaseCustomCallbacks) {
        val servicesDatabaseReference = database.getReference(SERVICES)
        var serviceNameList: ArrayList<String> = arrayListOf()
        val userListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (serviceName in dataSnapshot.children) {
                    serviceName.key?.let {
                        serviceNameList.add(it)
                    }
                }
                callback.provideServices(serviceNameList.toList())
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        servicesDatabaseReference.addValueEventListener(userListener)
    }

    fun saveService(serviceModel: ServiceModel, callbacks: FirebaseCustomCallbacks) {
        val servicesDatabaseReference = database.getReference(SERVICES).child(serviceModel.title)

        for (user in serviceModel.userModel) {

            user?.let {
                val emailAfterUsingDelimiter = it.emailAddress?.split("@",".","#","$","[","]")?.get(0) ?: ""
                servicesDatabaseReference.child("user")
                    .child(emailAfterUsingDelimiter).setValue(user)
            }
        }
        callbacks.genericCallback(DataResource.Success(data = null))
    }

    fun getServiceProvider(serviceName: String, callbacks: FirebaseCustomCallbacks) {
        val servicesDatabaseReference =
            database.getReference(SERVICES).child(serviceName).child("user")
        var serviceProviderList: ArrayList<UserModel?> = arrayListOf()
        val serviceProviderListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (user in dataSnapshot.children) {
                    serviceProviderList.add(user.getValue(UserModel::class.java))
                }
                callbacks.getServiceProvider(DataResource.Success(data = serviceProviderList.toList()))
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        servicesDatabaseReference.addValueEventListener(serviceProviderListener)

    }

}