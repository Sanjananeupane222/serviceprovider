package com.example.handy.firebase

import com.example.handy.ui.dashboard.ui.home.model.ServiceModel
import com.example.handy.ui.register.model.UserModel
import com.example.handy.util.networkUtils.DataResource

interface FirebaseCustomCallbacks {
   fun genericCallback(result:DataResource<String>){}
   fun getUserDetails(userDetails:UserModel){}
   fun provideServices(serviceNameList :List<String>){}
   fun getServiceProvider(providers:DataResource<List<UserModel?>>){}
}