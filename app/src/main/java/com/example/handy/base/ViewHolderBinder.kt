package com.example.handy.base

interface ViewHolderBinder {
    fun bind(position: Int)
}