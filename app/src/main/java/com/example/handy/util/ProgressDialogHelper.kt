package com.example.handy.util


import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.example.handy.R


object ProgressDialogHelper {

    fun getProgressDialog(context: Context): Dialog {
        return Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(false)
            window?.apply {
                setBackgroundDrawable(
                    ColorDrawable(Color.TRANSPARENT)
                )
                setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT
                )
            }
        }
    }
}

fun Dialog.showProgress(message: String = "Please Wait...") {
    if (!isShowing) {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_progress_dialog, null)
        (view.findViewById(R.id.tvMessage) as TextView).text = message
        setContentView(view)
        show()
    }
}

fun Dialog.dismissProgress() {
    if (isShowing) {
        dismiss()
    }
}