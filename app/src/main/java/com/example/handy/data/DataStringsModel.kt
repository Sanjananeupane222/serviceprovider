package com.example.handy.data

import androidx.databinding.ObservableField

object DataStringsModel {
    // Messages
    var ok = ObservableField("Ok")
    var save = ObservableField("Save")
    var cancel = ObservableField("Cancel")
    val notNow = ObservableField("Not now")
    val settings = ObservableField("Settings")

}