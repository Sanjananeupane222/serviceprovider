package com.example.handy.ui.register

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationRequest
import android.os.Bundle
import android.os.Looper
import androidx.activity.viewModels
import com.example.handy.R
import com.example.handy.base.BaseActivity
import com.example.handy.databinding.ActivityRegisterBinding
import com.example.handy.firebase.FirebaseCustomCallbacks
import com.example.handy.firebase.FirebaseHelper
import com.example.handy.ui.login.LoginActivity
import com.example.handy.ui.register.model.LocationModel
import com.example.handy.ui.register.model.UserModel
import com.example.handy.util.PermissionUtilities
import com.example.handy.util.ProgressDialogHelper
import com.example.handy.util.dismissProgress
import com.example.handy.util.extensions.showToast
import com.example.handy.util.extensions.transitionActivityStart
import com.example.handy.util.networkUtils.DataResource
import com.example.handy.util.showProgress
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject




@AndroidEntryPoint
class RegisterActivity : BaseActivity<ActivityRegisterBinding, RegisterViewModel>() {

    private val registerViewModel: RegisterViewModel by viewModels()

    @Inject
    lateinit var firebaseHelper: FirebaseHelper

    lateinit var createUserCallback: FirebaseCustomCallbacks

    lateinit var mFusedLocationClient: FusedLocationProviderClient

    private val loadingProgressBar by lazy {
        ProgressDialogHelper.getProgressDialog(this)
    }

    companion object {
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, RegisterActivity::class.java)
                })
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_register

    override fun getViewModel(): RegisterViewModel = registerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        intiViews()
        initCallbacks()
        initObserver()
        getLastLocation()
    }

    private fun getLastLocation() {
        if (PermissionUtilities.checkLocationPermission(this)) {
            if (PermissionUtilities.isLocationEnabled(this)) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        registerViewModel.location = LocationModel(
                            location.latitude,
                            location.longitude
                        )
                    }
                }
            }
        }else{
            PermissionUtilities.requestPermissions(this)
        }
    }

    private fun initCallbacks() {
        createUserCallback = object : FirebaseCustomCallbacks {
            override fun genericCallback(result: DataResource<String>) {
                when (result) {
                    is DataResource.Success -> {
                        with(registerViewModel) {
                            firebaseHelper.saveUser(
                                UserModel(
                                    firstName = firstName.get(),
                                    lastName = lastName.get(),
                                    phoneNumber = phoneNumber.get(),
                                    emailAddress = emailAddress.get(),
                                    role = role.get(),
                                    gender = gender.get(),
                                    password = null,
                                    location = registerViewModel.location
                                )
                            )
                        }
                        loadingProgressBar.dismissProgress()
                        showToast("User created successfully")
                        launchLoginActivity()
                    }
                    is DataResource.Error -> {
                        loadingProgressBar.dismissProgress()
                        showToast(result.message)
                    }
                }
            }
        }
    }

    private fun intiViews() {
        with(viewDataBinding) {
            with(toolbar) {
                ibBackArrow.setOnClickListener {
                    onBackPressed()
                }
            }
            tvSignIn.setOnClickListener {
                launchLoginActivity()
            }
        }
    }

    private fun launchLoginActivity() {
        LoginActivity.start(this)
        finish()
        transitionActivityStart()
    }

    private fun initObserver() {
        with(registerViewModel) {
            textFieldVerificationResponse.observe(this@RegisterActivity) {
                when (it) {
                    is DataResource.Success -> {
                        loadingProgressBar.showProgress()
                        firebaseHelper.signUpWithEmail(
                            UserModel(
                                firstName = null,
                                lastName = null,
                                role = null,
                                gender = null,
                                phoneNumber = null,
                                emailAddress = emailAddress.get(),
                                password = password.get(),
                                location = registerViewModel.location
                            ),
                            createUserCallback
                        )
                    }
                    is DataResource.Error -> {
                        showToast(it.message)
                    }
                    else -> {}
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PermissionUtilities.LOCATION_PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            registerViewModel.location = LocationModel(
                mLastLocation.latitude,
                mLastLocation.longitude
            )

        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = com.google.android.gms.location.LocationRequest()
        mLocationRequest.priority =
            com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        Looper.myLooper()?.let {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                it
            )
        }
    }
}
