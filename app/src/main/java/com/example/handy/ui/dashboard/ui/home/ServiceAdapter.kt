package com.example.handy.ui.dashboard.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.handy.databinding.ItemServiceBinding
import com.example.handy.ui.dashboard.ui.home.model.ServiceModel

class ServiceAdapter(
    private val serviceList: List<String>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<ServiceAdapter.JobViewHOlder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHOlder {
        val binding = ItemServiceBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return JobViewHOlder(binding)
    }

    override fun getItemCount() = serviceList.size

    override fun onBindViewHolder(holder: JobViewHOlder, position: Int) {
        with(holder) {
            binding.tvServiceItemName.text = serviceList[position]
            binding.cvService.setOnClickListener {
                listener.onItemClick(serviceList[position])
            }
        }
    }

    inner class JobViewHOlder(val binding: ItemServiceBinding) :
        RecyclerView.ViewHolder(binding.root)

}
