package com.example.handy.ui.serviceProviders

import androidx.databinding.ObservableField
import com.example.handy.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class ServiceProviderViewModel @Inject constructor(

) : BaseViewModel() {


    var userName = ObservableField("")
    var userPhoneNumber = ObservableField("")
    var serviceProviderTitle = ObservableField("")

}