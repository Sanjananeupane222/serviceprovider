package com.example.handy.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.example.handy.R
import com.example.handy.base.BaseActivity
import com.example.handy.databinding.ActivityMainBinding
import com.example.handy.ui.login.LoginActivity
import com.example.handy.ui.register.RegisterActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private val mainViewModel: MainViewModel by viewModels()

    companion object {
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, MainActivity::class.java)
                })
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun getViewModel(): MainViewModel = mainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intiViews()
    }

    private fun intiViews() {
        with(viewDataBinding){
            btnLogin.setOnClickListener {
                LoginActivity.start(this@MainActivity)
            }
            btnRegister.setOnClickListener {
                RegisterActivity.start(this@MainActivity)
            }
        }
    }

}
