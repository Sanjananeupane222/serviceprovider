package com.example.handy.ui.dashboard.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.handy.databinding.FragmentHomeBinding
import com.example.handy.firebase.FirebaseCustomCallbacks
import com.example.handy.firebase.FirebaseHelper
import com.example.handy.ui.dashboard.ui.home.model.ServiceModel
import com.example.handy.ui.register.model.UserModel
import com.example.handy.ui.serviceProviders.ServiceProviderActivity
import com.example.handy.util.extensions.showToast
import com.example.handy.util.extensions.transitionActivityStart
import com.example.handy.util.networkUtils.DataResource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment : Fragment() {

    @Inject lateinit var firebaseHelper: FirebaseHelper

    lateinit var firebaseCustomCallbacks: FirebaseCustomCallbacks

    private var _binding: FragmentHomeBinding? = null

    private lateinit var adapter: ServiceAdapter


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeViewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCallbacks()

        firebaseHelper.getServices(firebaseCustomCallbacks)
    }

    private fun initCallbacks() {
        firebaseCustomCallbacks = object:FirebaseCustomCallbacks{

            override fun provideServices(serviceNameList: List<String>) {
                setAdapter(serviceNameList)
            }

        }
    }

    private fun setAdapter(serviceList: List<String>) {
        adapter = ServiceAdapter(serviceList, object : OnItemClickListener {
            override fun onItemClick(serviceName: String) {
                ServiceProviderActivity.start(requireActivity(),serviceName)
                requireActivity().transitionActivityStart()
            }

        })
        binding.recyclerView.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}