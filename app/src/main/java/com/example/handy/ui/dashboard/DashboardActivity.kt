package com.example.handy.ui.dashboard

import android.app.Dialog
import android.os.Bundle
import android.view.Display
import android.view.View
import android.view.Window
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.handy.R
import com.example.handy.databinding.ActivityDashboardBinding
import com.example.handy.firebase.FirebaseCustomCallbacks
import com.example.handy.firebase.FirebaseHelper
import com.example.handy.ui.dashboard.ui.home.model.ServiceModel
import com.example.handy.ui.register.model.UserModel
import com.example.handy.util.ProgressDialogHelper
import com.example.handy.util.dismissProgress
import com.example.handy.util.extensions.showToast
import com.example.handy.util.extensions.transitionActivityStart
import com.example.handy.util.networkUtils.DataResource
import com.example.handy.util.showProgress
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class DashboardActivity : AppCompatActivity() {

    private val dashboardViewModel: DashboardViewModel by viewModels()

    private lateinit var binding: ActivityDashboardBinding

    private lateinit var firebaseCustomCallbacks: FirebaseCustomCallbacks

    private var userModel: UserModel? = null

    private lateinit var dialog :Dialog

    @Inject
    lateinit var firebaseHelper: FirebaseHelper

    private val email by lazy {
        intent.getStringExtra(EMAIL) ?: ""
    }

    companion object {
        const val EMAIL = "email"
    }



    private val loadingProgressBar by lazy {
        ProgressDialogHelper.getProgressDialog(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = Dialog(this)
        initCallbackListener()
        loadingProgressBar.showProgress("Fetching Services. Please wait...")
        getUserDetails()

        binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        binding.apply {
            viewModel = dashboardViewModel
            fab.setOnClickListener {
                showAddServiceDialog()
                transitionActivityStart()
            }
        }

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_dashboard)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications
            )
        )
        navView.setupWithNavController(navController)
        initObserver()
    }

    private fun initObserver() {

    }


    private fun showAddServiceDialog() {
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.activity_add_service)
        val title = dialog.findViewById(R.id.etServiceTitle) as TextInputEditText
        val cancelButton = dialog.findViewById(R.id.btn_cancel) as AppCompatButton
        val saveButton = dialog.findViewById(R.id.btn_save) as AppCompatButton
        cancelButton.setOnClickListener {
            dialog.dismiss()
        }
        saveButton.setOnClickListener {
            dialog.dismiss()
            loadingProgressBar.showProgress("Saving service. Please Wait..")
            var title =
                title.text?.substring(0, 1)?.uppercase(Locale.getDefault()) + title.text
                    ?.substring(1)
                    ?.lowercase(Locale.getDefault())
            if (title.isNotEmpty()) {
                firebaseHelper.saveService(
                    ServiceModel(
                        title = title,
                        userModel = listOf(userModel)
                    ),
                    firebaseCustomCallbacks
                )
            } else {
                showToast("Please add a service title.")
            }
        }
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val dialogWindow = dialog.window
        val m = windowManager
        val d: Display = m.defaultDisplay
        val lp = dialogWindow!!.attributes
        lp.width = (d.width * 0.9).toInt()

        dialog.show()

    }

    private fun initCallbackListener() {
        firebaseCustomCallbacks = object : FirebaseCustomCallbacks {
            override fun genericCallback(result: DataResource<String>) {
                loadingProgressBar.dismissProgress()
            }

            override fun getUserDetails(userDetails: UserModel) {
                loadingProgressBar.dismissProgress()
                dashboardViewModel.role.set(userDetails.role)
                userModel = userDetails
            }
        }
    }

    private fun getUserDetails() {
        firebaseHelper.readUser(email, firebaseCustomCallbacks)
    }

    private fun setupToolbar() {
        with(binding.toolbar) {
            ibBackArrow.visibility = View.GONE
        }
    }
}