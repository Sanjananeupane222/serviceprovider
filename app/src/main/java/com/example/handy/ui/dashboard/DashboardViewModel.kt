package com.example.handy.ui.dashboard

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.handy.base.BaseViewModel
import com.example.handy.ui.register.RegisterViewModel
import com.example.handy.ui.register.model.UserModel
import com.example.handy.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.*
import javax.inject.Inject


@HiltViewModel
class DashboardViewModel @Inject constructor(
) : BaseViewModel() {

    var serviceTitle = ObservableField("")
    var userModel: UserModel? = null

    private val _serviceTitleFieldValidator: MediatorLiveData<DataResource<String>> =
        MediatorLiveData()
    val serviceTitleFieldValidator: LiveData<DataResource<String>> get() = _serviceTitleFieldValidator

    var role = ObservableField(RegisterViewModel.Role.ROLE_CUSTOMER.role)



}