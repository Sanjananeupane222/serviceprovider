package com.example.handy.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.example.handy.R
import com.example.handy.base.BaseActivity
import com.example.handy.databinding.ActivityLoginBinding
import com.example.handy.firebase.FirebaseCustomCallbacks
import com.example.handy.firebase.FirebaseHelper
import com.example.handy.ui.dashboard.DashboardActivity
import com.example.handy.ui.register.RegisterActivity
import com.example.handy.util.ProgressDialogHelper
import com.example.handy.util.dismissProgress
import com.example.handy.util.extensions.showToast
import com.example.handy.util.extensions.transitionActivityStart
import com.example.handy.util.extensions.transitionFadeOut
import com.example.handy.util.networkUtils.DataResource
import com.example.handy.util.showProgress
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {

    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var verificationCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private lateinit var signInCallback: FirebaseCustomCallbacks

    @Inject
    lateinit var firebaseHelper: FirebaseHelper

    @Inject
    lateinit var mAuth: FirebaseAuth

    private val loadingProgressBar by lazy {
        ProgressDialogHelper.getProgressDialog(this)
    }


    companion object {
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, LoginActivity::class.java)
                })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth.firebaseAuthSettings.forceRecaptchaFlowForTesting(true)
        setStatusBarColor()
        initCallbacks()
        initViews()
        initObserver()
    }

    private fun initObserver() {
        with(loginViewModel) {
            isValidPhoneNumber.observe(this@LoginActivity) { isValidPhoneNumber ->
                when (isValidPhoneNumber) {
                    is DataResource.Success -> {
                        loadingProgressBar.dismissProgress()
                        isValidPhoneNumber.data?.let { number ->
                            loadingProgressBar.showProgress()
                            firebaseHelper.verifyPhoneNumber(
                                number,
                                this@LoginActivity,
                                verificationCallbacks
                            )
                        }
                    }
                    is DataResource.Error -> {
                        loadingProgressBar.dismissProgress()
                        showToast(isValidPhoneNumber.message)
                    }
                    is DataResource.Loading -> {
                        loadingProgressBar.showProgress()
                    }
                    else -> {}
                }
            }

            isValidEmailAddress.observe(this@LoginActivity) { isValidEmailAddress ->
                when (isValidEmailAddress) {
                    is DataResource.Success -> {
                        loadingProgressBar.showProgress()
                        firebaseHelper.signInWithEmailAndPassword(
                            emailAddress.get().toString(),
                            password.get().toString(),
                            signInCallback
                        )

                    }
                    is DataResource.Error -> {
                        loadingProgressBar.dismissProgress()
                        showToast(isValidEmailAddress.message)
                    }
                    is DataResource.Loading -> {
                        loadingProgressBar.showProgress()
                    }
                    else -> {}
                }
            }

        }
    }

    private fun initCallbacks() {
        verificationCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                loadingProgressBar.dismissProgress()
                firebaseHelper.signInWithPhoneAuthCredential(
                    p0,
                    this@LoginActivity,
                    signInCallback
                )
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                loadingProgressBar.dismissProgress()
                showToast(p0.message)
            }

            override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                loadingProgressBar.dismissProgress()
                Log.v("LoginActivity", "Verification token : $p0")
            }

        }

        signInCallback = object : FirebaseCustomCallbacks {
            override fun genericCallback(result: DataResource<String>) {
                when (result) {
                    is DataResource.Success -> {
                        val intent = Intent(this@LoginActivity, DashboardActivity::class.java)
                        intent.putExtra(DashboardActivity.EMAIL, result.data)
                        startActivity(intent)
                        finish()
                        transitionFadeOut()
                        loadingProgressBar.dismissProgress()
                    }
                    else -> {
                        showToast(result.toString())
                    }
                }
            }
        }
    }

    private fun initViews() {
        viewDataBinding.run {
            with(toolbar) {
                ibBackArrow.setOnClickListener {
                    onBackPressed()
                }
            }
            tvSignup.setOnClickListener {
                RegisterActivity.start(this@LoginActivity)
                finish()
                transitionActivityStart()
            }
        }
    }

    private fun setStatusBarColor() {
        val window: Window = window

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }


    override fun getLayoutId(): Int = R.layout.activity_login

    override fun getViewModel(): LoginViewModel = loginViewModel


}
