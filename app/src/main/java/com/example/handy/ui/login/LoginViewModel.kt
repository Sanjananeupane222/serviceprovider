package com.example.handy.ui.login

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.handy.R
import com.example.handy.base.BaseViewModel
import com.example.handy.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(

) : BaseViewModel() {
    var signInMode = ObservableField(SignInMode.MODE_NUMBER.mode)

    var userPhoneNumber = ObservableField("")
    var emailAddress = ObservableField("")
    var password  = ObservableField("")
    private val _isValidPhoneNumber: MediatorLiveData<DataResource<String>> = MediatorLiveData()
    val isValidPhoneNumber: LiveData<DataResource<String>> get() = _isValidPhoneNumber

    private val _isValidEmailAddress: MediatorLiveData<DataResource<String>> = MediatorLiveData()
    val isValidEmailAddress: LiveData<DataResource<String>> get() = _isValidEmailAddress

    fun onLoginClick() {
       if(signInMode.get().equals(SignInMode.MODE_NUMBER.mode)){
           _isValidPhoneNumber.postValue(DataResource.Loading)
           if (userPhoneNumber.get().isNullOrEmpty() || userPhoneNumber.get()?.length != 10) {
               _isValidPhoneNumber.postValue(DataResource.Error(message = "Please provide a valid phone number"))
           } else {
               _isValidPhoneNumber.postValue(DataResource.Success(data = "+977${userPhoneNumber.get()}"))
           }
       }else{
           if(emailAddress.get().isNullOrEmpty() || password.get().isNullOrEmpty()){
               _isValidEmailAddress.postValue(DataResource.Error(message = "Please provide a valid email address"))
           }else{
               _isValidEmailAddress.postValue(DataResource.Success(data = null))
           }
       }
    }

    fun onModeChange(id:Int){
        when(id){
            R.id.rb_phone_number->{
                signInMode.set(SignInMode.MODE_NUMBER.mode)
            }
            R.id.rb_email->{
                signInMode.set(SignInMode.MODE_EMAIL.mode)
            }
        }
    }

    enum class SignInMode(val mode:String){
        MODE_NUMBER("number_login"),
        MODE_EMAIL("email_login")
    }


}