package com.example.handy.ui.serviceProviders

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.handy.databinding.ItemServiceBinding
import com.example.handy.databinding.ItemUserBinding
import com.example.handy.ui.dashboard.ui.home.model.ServiceModel
import com.example.handy.ui.register.model.UserModel

class ServiceProviderAdapter(
    private val serviceProviderList: List<UserModel?>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<ServiceProviderAdapter.ServiceProviderViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceProviderViewHolder {
        val binding = ItemUserBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ServiceProviderViewHolder(binding)
    }

    override fun getItemCount() = serviceProviderList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ServiceProviderViewHolder, position: Int) {
        with(holder) {
            serviceProviderList[position]?.let {user->
                binding.tvUserName.text = "${user.firstName} ${user.lastName} "
                binding.tvPhoneNumber.text = user.phoneNumber
                binding.cvMakeACall.setOnClickListener {
                    listener.onMakeACallClick(user.phoneNumber?:"")
                }
            }
        }
    }

    inner class ServiceProviderViewHolder(val binding: ItemUserBinding) :
        RecyclerView.ViewHolder(binding.root)

}
