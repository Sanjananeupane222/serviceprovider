package com.example.handy.ui.register.model

import com.google.firebase.database.PropertyName

data class LocationModel(
    @PropertyName("latitude")
    val latitude:Double=0.0,
    @PropertyName("longitude")
    val longitude:Double=0.0
){
    constructor():this(0.0,0.0)
}
