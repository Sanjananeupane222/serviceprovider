package com.example.handy.ui.main

import com.example.handy.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class MainViewModel @Inject constructor(

) : BaseViewModel() {

}