package com.example.handy.ui.dashboard.ui.home.model

import com.example.handy.ui.register.model.UserModel
import com.google.firebase.database.PropertyName
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ServiceModel(
    @PropertyName("serviceTitle")
    val title:String,
    @PropertyName("user")
    val userModel: List<UserModel?>
){
    constructor() :this("", listOf())
}
