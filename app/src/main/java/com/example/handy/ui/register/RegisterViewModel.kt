package com.example.handy.ui.register

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.handy.R
import com.example.handy.base.BaseViewModel
import com.example.handy.firebase.FirebaseHelper
import com.example.handy.ui.register.model.LocationModel
import com.example.handy.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class RegisterViewModel @Inject constructor(
) : BaseViewModel() {

    var firstName = ObservableField("")
    var lastName = ObservableField("")
    var emailAddress = ObservableField("")
    var gender = ObservableField(Gender.GENDER_MALE.gender)
    var role = ObservableField(Role.ROLE_CUSTOMER.role)
    var phoneNumber = ObservableField("")
    var password = ObservableField("")
    var confirmPassword = ObservableField("")
    var location : LocationModel = LocationModel(0.0,0.0)


    private val _textFieldVerificationResponse : MediatorLiveData<DataResource<String>> = MediatorLiveData()
    val textFieldVerificationResponse:LiveData<DataResource<String>> get() = _textFieldVerificationResponse



    fun onGenderChanged(id: Int) {
        when (id) {
            R.id.rb_male -> {
                gender.set(Gender.GENDER_MALE.gender)
            }
            R.id.rb_female -> {
                gender.set(Gender.GENDER_FEMALE.gender)
            }
            R.id.rb_others -> {
                gender.set(Gender.GENDER_OTHERS.gender)
            }
        }
    }

    fun onRoleChanged(id: Int) {
        when (id) {
            R.id.rb_customer -> {
                role.set(Role.ROLE_CUSTOMER.role)
            }
            R.id.rb_service_provider -> {
                role.set(Role.ROLE_SERVICE_PROVIDER.role)
            }
        }
    }

    fun onProceedClicked() {
        if (firstName.get().isNullOrEmpty().not()
            && lastName.get().isNullOrEmpty().not()
            && emailAddress.get().isNullOrEmpty().not()
            && phoneNumber.get().isNullOrEmpty().not()
        ){
            if(password.get().equals(confirmPassword.get()).not()){
                _textFieldVerificationResponse.postValue(DataResource.Error(message = "Password and confirm password doesn't match."))
            }else{
                _textFieldVerificationResponse.postValue(DataResource.Success())
            }
        }
        else{
            _textFieldVerificationResponse.postValue(DataResource.Error(message = "Please fill all the credentials."))
        }
    }


    enum class Gender(val gender: String) {
        GENDER_MALE("male"),
        GENDER_FEMALE("female"),
        GENDER_OTHERS("others")
    }

    enum class Role(val role: String) {
        ROLE_CUSTOMER("customer"),
        ROLE_SERVICE_PROVIDER("service_provider")
    }
}