package com.example.handy.ui.serviceProviders

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import com.example.handy.R
import com.example.handy.base.BaseActivity
import com.example.handy.databinding.ActivityServiceProviderBinding
import com.example.handy.firebase.FirebaseCustomCallbacks
import com.example.handy.firebase.FirebaseHelper
import com.example.handy.ui.register.model.UserModel
import com.example.handy.util.networkUtils.DataResource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ServiceProviderActivity : BaseActivity<ActivityServiceProviderBinding, ServiceProviderViewModel>() {

    private val serviceProviderViewModel: ServiceProviderViewModel by viewModels()

    @Inject
    lateinit var firebaseHelper: FirebaseHelper

    lateinit var firebaseCustomCallbacks: FirebaseCustomCallbacks

    lateinit var adapter:ServiceProviderAdapter


    private val serviceName by lazy {
        intent.getStringExtra(SERVICE_NAME)
    }

    companion object {
         private const val SERVICE_NAME = "service_name"
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, ServiceProviderActivity::class.java)
                })
            }
        }

        fun start(context: Context, serviceName:String) {
            context.run {
                startActivity(Intent().apply {
                    putExtra(SERVICE_NAME,serviceName)
                    setClass(this@run, ServiceProviderActivity::class.java)
                })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setupCallback()
        getServiceProviderList()
    }

    private fun initViews() {
        serviceProviderViewModel.serviceProviderTitle.set("$serviceName in your city")
    }

    private fun setupCallback() {
        firebaseCustomCallbacks = object:FirebaseCustomCallbacks{
            override fun getServiceProvider(providers: DataResource<List<UserModel?>>) {
                when(providers){
                    is DataResource.Success->{
                       providers.data?.let { details->
                           setAdapter(details)
                       }
                    }
                }
            }
        }
    }

    private fun setAdapter(details: List<UserModel?>) {
        adapter = ServiceProviderAdapter(details, object : OnItemClickListener {
            override fun onMakeACallClick(phoneNumber: String) {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:$phoneNumber")
                startActivity(intent)
            }

        })
        viewDataBinding.recyclerView.adapter = adapter
    }

    private fun getServiceProviderList() {
       firebaseHelper.getServiceProvider(serviceName?:"",firebaseCustomCallbacks)
    }

    override fun getLayoutId(): Int = R.layout.activity_service_provider

    override fun getViewModel(): ServiceProviderViewModel = serviceProviderViewModel

}
