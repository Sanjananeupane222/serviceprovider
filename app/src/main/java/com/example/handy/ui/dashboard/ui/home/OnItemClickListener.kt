package com.example.handy.ui.dashboard.ui.home

import com.example.handy.ui.dashboard.ui.home.model.ServiceModel


interface OnItemClickListener {
    fun onItemClick(service: String)
}