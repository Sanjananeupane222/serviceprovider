package com.example.handy.ui.serviceProviders

import com.example.handy.ui.dashboard.ui.home.model.ServiceModel


interface OnItemClickListener {
    fun onItemClick(service: String){}
    fun onMakeACallClick(phoneNumber:String)
}