package com.example.handy.ui.register.model


import com.google.firebase.database.PropertyName


data class UserModel(
    @PropertyName("firstName")
    val firstName:String?,
    @PropertyName("lastName")
    val lastName:String?,
    @PropertyName("emailAddress")
    val emailAddress:String?,
    @PropertyName("phoneNumber")
    val phoneNumber:String?,
    @PropertyName("gender")
    val gender:String?,
    @PropertyName("password")
    val password:String?,
    @PropertyName("role")
    val role:String?,
    @PropertyName("location")
    val location : LocationModel?

){
    constructor():this(null,null,null,null,null,null,null,null)
}
