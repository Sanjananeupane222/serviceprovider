package com.example.handy.di

import android.app.Activity
import android.content.Context
import com.example.handy.firebase.FirebaseHelper
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.introdating.app.app.HandyMyanApp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Nabin Shrestha on 8/24/21.
 */

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    fun provideApplication(@ApplicationContext app: Context): HandyMyanApp {
        return app as HandyMyanApp
    }


    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Provides
    @Singleton
    fun provideFirebaseDatabase(): FirebaseDatabase = FirebaseDatabase.getInstance()

    @Provides
    @Singleton
    fun provideFirebaseHelper(

        mAuth: FirebaseAuth,
        database: FirebaseDatabase
    ): FirebaseHelper = FirebaseHelper(mAuth, database)


}

