package com.example.handy.sample

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.example.handy.R
import com.example.handy.base.BaseActivity
import com.example.handy.databinding.ActivitySampleBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SampleActivity : BaseActivity<ActivitySampleBinding, SampleViewModel>() {

    private val loginViewModel: SampleViewModel by viewModels()

    companion object {
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, SampleActivity::class.java)
                })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun getLayoutId(): Int = R.layout.activity_sample

    override fun getViewModel(): SampleViewModel = loginViewModel

}
