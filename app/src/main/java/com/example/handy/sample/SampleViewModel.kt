package com.example.handy.sample

import com.example.handy.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class SampleViewModel @Inject constructor(

) : BaseViewModel() {

}