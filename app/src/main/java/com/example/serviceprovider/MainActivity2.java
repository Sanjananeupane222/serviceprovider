package com.example.serviceprovider;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.serviceprovider.Home.HomeFragmentSp;
import com.example.serviceprovider.databinding.ActivityMain2Binding;
import com.example.serviceprovider.setting.SettingFragmentSp;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity2 extends AppCompatActivity {


    ActivityMain2Binding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportFragmentManager().beginTransaction().replace(R.id.container_sp,new HomeFragmentSp()).commit();
        binding.bottomnavSp.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                Fragment fragment = null;
                switch (item.getItemId()){
                    case R.id.navigation_home1:
                        fragment = new HomeFragmentSp();
                        break;


                    case R.id.navigation_notifications_sp:
                        fragment = new SettingFragmentSp();
                        break;


                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container_sp,fragment).commit();
                return true;
            }



            });

    }
}